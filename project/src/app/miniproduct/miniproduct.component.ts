import { ProdutsService } from './../shared/services/produts.service';
import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../shared/models/Product';

@Component({
  selector: 'app-miniproduct',
  templateUrl: './miniproduct.component.html',
  styleUrls: ['./miniproduct.component.scss']
})
export class MiniproductComponent implements OnInit {
  @Input() product: Product;
  @Input() darker: Boolean;

  constructor(private productService: ProdutsService) { }

  ngOnInit() {
  }

  removeFromCart(e, id) {
    e.preventDefault();
    console.log(e);
    this.productService.toggleCartById(id);
  }

}
