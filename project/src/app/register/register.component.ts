import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  errors: String[] = [];
  constructor(
    private auth: AuthService,
    private router: Router
    ) { }

  ngOnInit() { }

  onSubmit({valid, value}: NgForm) {
    if (valid) {
      this.auth.signup(value)
          .subscribe(
            ({email}) => {
              this.router.navigate(['/signin'], {queryParams: {email}});
            },
            ({errors, user}) => {
              this.errors = errors;
              this.form.setValue(user);
            }
          );
    } else {
      this.errors = ['Invalid data'];
    }
  }

}
