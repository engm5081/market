import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss']
})
export class RateComponent implements OnInit {
  @Input() rate: any;
  fixedRate: number;
  constructor() { }

  ngOnInit() {
    this.fixedRate = this.rate.toFixed(1);
  }

}
