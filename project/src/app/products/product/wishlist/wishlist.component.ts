import { Component, OnInit, Input } from '@angular/core';
import { ProdutsService } from '../../../shared/services/produts.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {
  @Input() item;
  constructor(private products: ProdutsService) { }

  toggleWishList() {
    this.products.toggleWishList(this.item);
  }


  ngOnInit() {
  }

}
