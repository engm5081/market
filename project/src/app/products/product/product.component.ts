import { Subscription } from 'rxjs';
import { ProdutsService } from './../../shared/services/produts.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Product } from '../../shared/models/Product';
import { AuthService } from '../../shared/services/auth.service';
import { User } from '../../shared/models/user';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  @Input() product: Product;
  removeActions: Boolean = false;
  inCart: Boolean = false;
  cartSubscription: Subscription;

  constructor(private productsService: ProdutsService, private auth: AuthService) { }

  onAddToCart(id) {
    this.productsService.toggleCartById(id);
  }

  public ngOnInit(): void {
    this.cartSubscription = this.productsService.cart$.subscribe(
      cart => {
        const index = cart.findIndex(product => product._id === this.product._id);
        this.inCart = index > -1;
        this.auth.auth$.pipe(take(1)).subscribe((user: User) => {
          if (!user) { return; }
          const isOwer = this.product.ownerId === user._id;
          this.removeActions = isOwer;
        });
      }
    );
  }

  public ngOnDestroy(): void {
    this.cartSubscription.unsubscribe();
  }

}
