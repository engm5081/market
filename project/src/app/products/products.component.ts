import { ActivatedRoute, Router } from '@angular/router';
import { ProdutsService } from './../shared/services/produts.service';
import { Product } from './../shared/models/Product';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  products: Product[];
  pages: number;
  id: number;
  countSubscription: Subscription;
  array: Array<Number>;
  constructor(
    private productsService: ProdutsService,
    private route: ActivatedRoute,
    private router: Router) { }


    selectPages(id) {
      if (id > this.pages) {
        this.router.navigateByUrl(`/products/${this.pages}`);
      } else if (this.pages === 2) {
        this.array = [1, 2];
      } else if (id === 1) {
        this.array = [0, 1, 2];
      } else if (this.pages - id === 0) {
        this.array = [-2, -1, 0];
      } else if (id > 1) {
        this.array = [-1, 0, 1];
      }
    }


    setPagesBar() {
      this.selectPages(this.id);
      const getPageProducts = this.productsService.getPageProducts(this.id);
      if (getPageProducts instanceof Observable) {
        getPageProducts.pipe(take(1)).subscribe(
          products => {
            this.products = products;
          }
        );
      } else {
        this.products = getPageProducts;
      }
      if (this.pages === 1) {
        return;
      }
    }

  public ngOnInit(): void {

    this.countSubscription = this.productsService.count$.subscribe(
        count => {
          if (count === null) {
            return;
          }
          this.pages = Math.ceil((count === 0 ? 1 : count) / 12);
          this.setPagesBar();
        }
      );

      this.route.params.subscribe(
        ({id}) => {
          this.id = +id;
          if (isNaN(this.id) || id < 1) {
            return this.router.navigateByUrl('/products/1');
          }
          this.setPagesBar();
        },
        _ => {
          this.id = (this.id || 1) + 1;
          this.selectPages(this.id);
        }
      );
  }

  public ngOnDestroy(): void {
    this.countSubscription.unsubscribe();
  }

}
