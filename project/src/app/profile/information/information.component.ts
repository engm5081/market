import { Component, Input } from '@angular/core';
import { User } from '../../shared/models/user';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent {
  @Input() user: User;

}
