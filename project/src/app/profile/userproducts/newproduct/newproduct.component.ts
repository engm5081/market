import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProdutsService } from '../../../shared/services/produts.service';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-newproduct',
  templateUrl: './newproduct.component.html',
  styleUrls: ['./newproduct.component.scss']
})
export class NewproductComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  file: File;
  src: String;
  errors: String[];
  uploadingImg: Boolean = false;
  value: Number = 0;
  constructor(private productsService: ProdutsService) { }

  ngOnInit() {
  }

  dragPrev() {
    return false;
  }

  fileReader(file) {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.src = `url('${e.target.result}')`;
    };
    reader.readAsDataURL(file);
  }

  uploadImg(e) {
    this.file = e.dataTransfer.files[0];
    const type = this.file.type;
    if (type.indexOf('image') === -1) {
      this.file = null;
      this.src = null;
      return;
    }
    this.fileReader(this.file);
    return false;
  }

  uploadImgInput(e) {
    this.file = e.target.files[0];
    const type = this.file.type;
    if (type.indexOf('image') === -1) {
      this.file = null;
      this.src = null;
      return;
    }
    this.fileReader(this.file);
  }

  removeImg() {
    this.src = null;
    this.file = null;
  }

  onSubmit({valid, value}: NgForm) {
    if ((this.file.size / 1024) / 1024 > 5) {
      return this.errors = ['Max file size is 5 MB'];
    }
    if (valid && this.file) {
      const fd = new FormData();
      fd.append('image', this.file, this.file.name);
      fd.append('product', JSON.stringify(value));
      fd.append('user', JSON.stringify({
        _id: localStorage.getItem('_id')
      }));
      this.productsService.addProduct(fd)
          .subscribe(
            res => {
              if (res.type === HttpEventType.UploadProgress) {
                this.uploadingImg = true;
                this.value = Math.floor((res.loaded / res.total) * 100);
                if (this.value === 100) {
                  this.uploadingImg = false;
                }
              } else if (res.type === HttpEventType.Response) {
                this.uploadingImg = false;
                this.form.resetForm();
                this.src = null;
                this.file = null;
              }
            },
            err => {
              this.uploadingImg = false;
              console.log(err);
            }
          );
    }
  }

}
