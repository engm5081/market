import { User } from './../../shared/models/user';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-userproducts',
  templateUrl: './userproducts.component.html',
  styleUrls: ['./userproducts.component.scss']
})
export class UserproductsComponent implements OnInit {
  @Input() user: User;
  @Input() id: String;

  constructor() { }

  ngOnInit() {
  }

}
