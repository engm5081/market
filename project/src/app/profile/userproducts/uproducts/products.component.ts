import { ActivatedRoute } from '@angular/router';
import { User } from './../../../shared/models/user';
import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../shared/models/Product';
import { ProdutsService } from '../../../shared/services/produts.service';

@Component({
  selector: 'app-uproducts',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class UProductsComponent implements OnInit {
  @Input() user: User;
  products: Product[];

  constructor(private productService: ProdutsService, private route: ActivatedRoute) { }

  public ngOnInit(): void {
    this.route.params.subscribe(({id}) => {
      this.productService.getMyProductsViaServer(id)
      .subscribe(
        ({products}) => {
          console.log(products);
          this.products = products;
        }
      );
  });
  }

}
