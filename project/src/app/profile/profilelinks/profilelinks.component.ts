import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../shared/models/user';

@Component({
  selector: 'app-profilelinks',
  templateUrl: './profilelinks.component.html',
  styleUrls: ['./profilelinks.component.scss']
})
export class ProfilelinksComponent implements OnInit {
  @Input() user: User;
  @Input() id: String;

  constructor() { }

  ngOnInit() {
  }

}
