import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { User } from './../shared/models/user';
import { AuthService } from './../shared/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  user: User;
  visitedUser: User;
  id: String;
  userSubscription: Subscription;
  newUserSubscription: Subscription;
  constructor(private auth: AuthService, private route: ActivatedRoute) { }

  public ngOnInit(): void {
    this.userSubscription = this.auth.auth$.subscribe(
      user => this.user = user
    );
    this.newUserSubscription = this.auth.visitedUser$.subscribe(
      visitedUser => {
        if (!visitedUser) {
          return;
        }
        this.visitedUser = visitedUser.user;
      }
    );
    this.route.params.subscribe(
      ({id}) => {
        this.id = id;
        if (this.id !== this.user._id) {
          this.auth.getUserById(this.id);
        }
      }
    );

  }

  public ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
    this.newUserSubscription.unsubscribe();
  }

}
