import { AuthService } from './../../shared/services/auth.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-usersettings',
  templateUrl: './usersettings.component.html',
  styleUrls: ['./usersettings.component.scss']
})
export class UsersettingsComponent implements OnInit {
  @ViewChild('cover') cover;
  @ViewChild('profile') profile;
  coverFile: File;
  profileFile: File;
  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  onChangeHandler(e, v) {
    console.log(e);
    this[v] = e.target.files[0];
    console.log(v, this[v]);
  }

  onSubmit() {
    const fd = new FormData();
    fd.append('cover', this.coverFile, this.coverFile.name);
    fd.append('profile', this.profileFile, this.profileFile.name);
    fd.append('id', localStorage.getItem('id'));
    this.auth.uploadUserImages(fd)
        .subscribe(
          done => console.log(done),
          _ => console.log('err')
        );
  }

}
