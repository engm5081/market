import { ProdutsService } from './../../shared/services/produts.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Product } from '../../shared/models/Product';

@Component({
  selector: 'app-usercart',
  templateUrl: './usercart.component.html',
  styleUrls: ['./usercart.component.scss']
})
export class UsercartComponent implements OnInit, OnDestroy {

  products: Product[];
  cartSubscribtion: Subscription;

  constructor(private productService: ProdutsService) { }

  public ngOnInit(): void {
    this.cartSubscribtion = this.productService.cart$
      .subscribe(
        products => {
          this.products = products;
        }
      );
  }

  public ngOnDestroy(): void {
    this.cartSubscribtion.unsubscribe();
  }

}
