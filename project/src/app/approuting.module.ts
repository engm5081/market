import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { LayoutComponent } from './layout/layout.component';
import { MaterialcomponentsModule } from './materialcomponents.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './shared/guards/auth.guard';
import { NotauthGuard } from './shared/guards/notauth.guard';
import { ProfileComponent } from './profile/profile.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { ProductComponent } from './products/product/product.component';
import { ProductdescPipe } from './shared/pipes/productdesc.pipe';
import { ProductsComponent } from './products/products.component';
import { RateComponent } from './products/product/rate/rate.component';
import { AccoutComponent } from './layout/accout/accout.component';
import { ColorsComponent } from './layout/colors/colors.component';
import { LinksComponent } from './layout/links/links.component';
import { WishlistComponent } from './products/product/wishlist/wishlist.component';
import { CartComponent } from './layout/cart/cart.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ProfileheaderComponent } from './profile/profileheader/profileheader.component';
import { InformationComponent } from './profile/information/information.component';
import { UserproductsComponent } from './profile/userproducts/userproducts.component';
import { UsersettingsComponent } from './profile/usersettings/usersettings.component';
import { ProfilelinksComponent } from './profile/profilelinks/profilelinks.component';
import { UsercartComponent } from './profile/usercart/usercart.component';
import { FooterComponent } from './footer/footer.component';
import { NewproductComponent } from './profile/userproducts/newproduct/newproduct.component';
import { UProductsComponent } from './profile/userproducts/uproducts/products.component';
import { AppInterceptor } from './shared/services/app.interceptor';
import { SpinnerComponent } from './spinner/spinner.component';
import { LoadingbarComponent } from './loadingbar/loadingbar.component';
import { MiniproductComponent } from './miniproduct/miniproduct.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


const appRoutes: Routes = [
  {component: HomeComponent, path: ''},
  {component: ProductsComponent, path: 'products/:id'},
  {component: RegisterComponent, path: 'signup', canActivate: [NotauthGuard]},
  {component: LoginComponent, path: 'signin', canActivate: [NotauthGuard]},
  {component: ProfileComponent, path: 'profile/:id', canActivateChild: [AuthGuard], canActivate: [AuthGuard]/* , children: [
    {component: InformationComponent, path: '', data: {depth: 1}},
    {component: UserproductsComponent, path: 'products', data: {depth: 2}, canActivateChild: [AuthGuard], children: [
      {component: UProductsComponent, path: ''},
      {component: NewproductComponent, path: 'new'}
    ]},
    {component: UsercartComponent, path: 'cart', data: {depth: 3}},
    {component: UsersettingsComponent, path: 'settings', data: {depth: 4}}
  ] */},
  {path: '**', component:  PagenotfoundComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes),
    MaterialcomponentsModule,
    FormsModule,
    HttpClientModule,
    PerfectScrollbarModule
  ],
  declarations: [
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    LayoutComponent,
    ProfileComponent,
    PagenotfoundComponent,
    ProductComponent,
    ProductdescPipe,
    ProductsComponent,
    RateComponent,
    AccoutComponent,
    ColorsComponent,
    LinksComponent,
    WishlistComponent,
    CartComponent,
    ProfileheaderComponent,
    InformationComponent,
    UserproductsComponent,
    UsersettingsComponent,
    ProfilelinksComponent,
    UsercartComponent,
    FooterComponent,
    NewproductComponent,
    UProductsComponent,
    SpinnerComponent,
    LoadingbarComponent,
    MiniproductComponent
  ],
  exports: [
    RouterModule,
    MaterialcomponentsModule,
    FormsModule,
    HttpClientModule,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    LayoutComponent,
    ProfileComponent,
    ProductsComponent
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true
    }
  ]
})
export class ApproutingModule { }
