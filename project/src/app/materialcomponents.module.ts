import { NgModule } from '@angular/core';
import {
  MatSidenavModule,
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCheckboxModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatGridListModule,
  MatCardModule,
  MatDividerModule,
  MatTooltipModule,
  MatProgressBarModule,
  MatButtonToggleModule,
  MatProgressSpinnerModule,
  MatTabsModule
} from '@angular/material';

const components = [
  MatSidenavModule,
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCheckboxModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatGridListModule,
  MatCardModule,
  MatDividerModule,
  MatProgressBarModule,
  MatTooltipModule,
  MatButtonToggleModule,
  MatProgressSpinnerModule,
  MatTabsModule
];


@NgModule({
  imports: components,
  exports: components
})
export class MaterialcomponentsModule { }
