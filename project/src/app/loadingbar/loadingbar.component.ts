import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loadingbar',
  templateUrl: './loadingbar.component.html',
  styleUrls: ['./loadingbar.component.scss']
})
export class LoadingbarComponent implements OnInit {
  @Input() value: Number;

  constructor() { }

  ngOnInit() {
  }

}
