import { ProdutsService } from './../shared/services/produts.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errors: String[] = [];
  email: String;

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private produtsService: ProdutsService) { }

  public ngOnInit(): void {
    this.email = this.route.snapshot.queryParams['email'] || '';
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  onSubmit({valid, value}: NgForm) {
    if (valid) {
      this.auth.signin(value)
          .subscribe(
            res => {
              for (const key in res) {
                if (key === 'cart') {
                  continue;
                }
                if (typeof res[key] === 'object' && res[key] instanceof Array) {
                  localStorage.setItem(key, JSON.stringify(res[key]));
                } else {
                  localStorage.setItem(key, res[key]);
                }
              }
              const currentCart = res.cart;
              let oldCart;
              try {
                oldCart = JSON.parse(localStorage.getItem('cart'));
                if (oldCart === null) {
                  throw new Error('fire catch');
                }
                oldCart = [...oldCart, ...currentCart];
                const cart = this.removeDuplicates(oldCart, '_id');
                // TODO: send cart to server
                this.produtsService.updateCartAfterLogin(cart)
                    .subscribe(
                      _ => localStorage.setItem('cart', JSON.stringify(cart)),
                      _ => localStorage.setItem('cart', JSON.stringify(cart))
                    );
              } catch (_) {
                localStorage.setItem('cart', JSON.stringify(currentCart));
              }
              this.auth.checkSignIn();
              this.router.navigateByUrl('/');
            },
            ({errors}) => {
              this.errors = errors ? errors : [];
            }
          );
    } else {
      this.errors = ['Invalid data'];
    }
  }

}
