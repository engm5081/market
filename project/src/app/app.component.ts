import { AuthService } from './shared/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ProdutsService } from './shared/services/produts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private auth: AuthService,
    private productService: ProdutsService) {}

  ngOnInit() {
    this.productService.getProductsNumber();
    this.auth.checkSignIn();
  }

}
