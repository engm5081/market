import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accout',
  templateUrl: './accout.component.html',
  styleUrls: ['./accout.component.scss']
})
export class AccoutComponent implements OnInit {
  @Input() user;
  constructor(
    private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  logout() {
    for (const key in localStorage) {
      if (this.user[key] !== undefined) {
        localStorage.removeItem(key);
      }
    }

    this.auth.checkSignIn();
    this.router.navigateByUrl('/');
  }

}
