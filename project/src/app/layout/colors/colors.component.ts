import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-colors',
  templateUrl: './colors.component.html',
  styleUrls: ['./colors.component.scss']
})
export class ColorsComponent implements OnInit {
  @Output('globalTheme') globalTheme: EventEmitter<any> = new EventEmitter<any>();

  colors: {color: String, className: String}[] = [
    {color: '#ffab00', className: 'amber'},
    {color: '#304ffe', className: 'indigo'},
    {color: '#00bfa5', className: 'teal'},
    {color: '#2962ff', className: 'blue'}
  ];

  classes: Object = {
    'mat-typography': true,
    'dark': false
  };

  changeMode() {
    this.classes['dark'] = !this.classes['dark'];
    localStorage.setItem('dark', this.classes['dark'] ? '1' : '0');
    this.overlayContainer.getContainerElement().classList[this.classes['dark'] ? 'add' : 'remove']('dark');
    this.globalTheme.emit(this.classes);
  }
  constructor(
    private overlayContainer: OverlayContainer
    ) { }


    changeTheme(index) {
      index = +index;
      let i;
      i = +localStorage.getItem('color') || 0;
      if (i === index) {
        return;
      }
      localStorage.setItem('color', index);
      let className: any = this.colors[index].className;
      this.classes[className] = true;
      console.log(className);
      for (const color of this.colors) {
        if (color.className === className) {
          this.overlayContainer.getContainerElement().classList.add(className);
        } else {
          const theme: any = color.className;
          this.overlayContainer.getContainerElement().classList.remove(theme);
        }
      }
      className = this.colors[i].className;
      this.classes[className] = false;
      this.globalTheme.emit(this.classes);
    }

    public ngOnInit(): void {
      const isDark  = localStorage.getItem('dark');
      if (!isDark) {
        localStorage.setItem('dark', '0');
      } else if (isDark === '1') {
        this.classes['dark'] = true;
        this.overlayContainer.getContainerElement().classList.add('dark');
      }
      const colors = this.colors;
      let className;
      for (let i = 0; i < colors.length; i++) {
        className = colors[i].className;
        this.classes[className] = false;
      }
      const clr = localStorage.getItem('color');
      if (!clr) {
        localStorage.setItem('color', '0');
      }
      className = this.colors[+clr || 0].className;
      this.classes[className] = true;
      this.overlayContainer.getContainerElement().classList.add(className);
      this.globalTheme.emit(this.classes);
    }

}
