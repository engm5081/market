import { ProdutsService } from './../../shared/services/produts.service';
import { Product } from './../../shared/models/Product';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CartComponent implements OnInit {
  products: Product[] = [];
  constructor(private productService: ProdutsService) { }

  ngOnInit() {
    this.productService.cart$.subscribe(
      products => {
        this.products = products;
      }
    );
  }

  clearCart() {
    this.productService.clearCart();
  }

}
