import { Component, ViewChild, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/services/auth.service';
import { Router, RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';
import { LoadingService } from '../shared/services/loading.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  @ViewChild('layoutScroll') layoutScroll: PerfectScrollbarComponent;
  width$: Observable<Boolean> = this.breakpointObserver.observe(Breakpoints.XSmall)
                                    .pipe(
                                      map((result: any) => result.matches)
                                    );
  opened: Boolean = false;
  loading: Boolean = false;
  classes: Object = {};
  user: any = {};
  constructor(
    private breakpointObserver: BreakpointObserver,
    private auth: AuthService,
    private router: Router,
    private appLoading: LoadingService) {
      this.router.events.subscribe((event: RouterEvent) => {
        this.navigationInterceptor(event);
      });
      this.auth.auth$.subscribe(
        user => {
          if (!user) {
            return this.user = {};
          }
          this.user = user;
        }
      );
    }

    public ngOnInit(): void {
      this.appLoading.state$.subscribe(
        state => this.loading = state
      );
    }

    changeTheme(classes) {
      this.classes = classes;
    }

    navigationInterceptor(event: RouterEvent): void {
      if (event instanceof NavigationStart) {
        this.appLoading.changeState(true);
      } else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel ||
          event instanceof NavigationError) {
            this.appLoading.changeState(false);
            this.layoutScroll.directiveRef.scrollToY(0, 300);
      }
    }

}
