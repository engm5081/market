import { LoadingService } from './loading.service';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { finalize, catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()

export class AppInterceptor implements HttpInterceptor {
  constructor(private loadingService: LoadingService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const copiedReq = req.clone({
      setHeaders: {
        'authorization': localStorage.getItem('token') || ''
      }
    });
    return next.handle(copiedReq).pipe(
      map(data => {
        this.loadingService.changeState(true);
        return data;
      }),
      catchError(err => {
        this.loadingService.changeState(false);
        return throwError(err.error);
      }),
      finalize(() => {
        this.loadingService.changeState(false);
      })
    );
  }
}
