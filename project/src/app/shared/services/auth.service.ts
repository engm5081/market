import { SigninUser } from './../models/user';
import { SignupUser } from '../models/user';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private _auth$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  auth$: Observable<any> = this._auth$.asObservable();

  private _visitedUser$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  visitedUser$: Observable<any> = this._visitedUser$.asObservable();
  private visitedUsers = {};

  isLogin: Boolean = false;
  timeOutContainer = null;

  constructor(private http: HttpClient) { }

  signup(user: SignupUser) { return this.http.post<any>('/user/signup', user); }

  signin(user: SigninUser) { return this.http.post<any>('/user/signin', user); }

  refreshToken() {
    return this.http.get<any>(`/token/refresh/${localStorage.getItem('_id') || ''}`).subscribe(({token, expiresIn}) => {
      localStorage.setItem('token', token);
      localStorage.setItem('expiresIn', expiresIn);
      this.checkTorefreshToken();
    },
    _ => this.checkTorefreshToken());
  }


  checkTorefreshToken() {
    if (this.isLogin) {
      if (isNaN(+localStorage.getItem('expiresIn'))) {
        return;
      }
      const expiresIn = (+localStorage.getItem('expiresIn') || 0) - new Date().getTime();
      if (expiresIn > 0) {
        this.timeOutContainer = setTimeout(() => {
          this.refreshToken();
        }, expiresIn);
      } else {
        this.refreshToken();
      }
    } else {
      clearTimeout(this.timeOutContainer);
      this.timeOutContainer = null;
    }
  }

  toggleUserLogin(user) {
    this._auth$.next(user);
    this.isLogin = user ? true : false;
    this.checkTorefreshToken();
  }

  checkSignIn() {
    const user = {
      cart: null,
      email: null,
      expiresIn: null,
      firstName: null,
      gender: null,
      lastName: null,
      token: null,
      wishList: null,
      _id: null,
      date: null
    };
    let valid = true;
    for (const key in user) {
      if (1) {
        try {
          user[key] = JSON.parse(localStorage.getItem(key));
        } catch (_) {
          user[key] = localStorage.getItem(key);
        }
        valid = valid && user[key] !== null;
      }
    }
    if (valid) {
      return this.toggleUserLogin(user);
    }
    this.toggleUserLogin(null);
    this.checkTorefreshToken();
  }

  getUserById(id) {
    if (this.visitedUsers[id]) {
      this._visitedUser$.next(this.visitedUsers[id]);
      return;
    }
    this.http.post<any>('/user/getById', {id})
              .subscribe(
                user => {
                  this.visitedUsers[user._id] = user;
                  this._visitedUser$.next(user);
                },
                _ => {}
              );
  }

  uploadUserImages(fd) {
    return this.http.post<any>('/user/images', fd);
  }

}
