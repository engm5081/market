import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { take, map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { Product } from '../models/Product';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProdutsService {

  private _cart$: BehaviorSubject < Product[] > = new BehaviorSubject < Product[] > ([]);
  private cart: Product[] = [];
  cart$: Observable < Product[] > = this._cart$.asObservable();

  private _count$: BehaviorSubject < number > = new BehaviorSubject < number > (null);
  private count = 0;
  count$: Observable < number > = this._count$.asObservable();


  private products: any /* @Include {?} Product[] */ = {};

  constructor(private http: HttpClient, private auth: AuthService) {
    try {
      this.cart = JSON.parse(localStorage.getItem('cart')) || [];
      this._cart$.next(this.cart);
    } catch (_) {}
  }

  getProductsNumber() {
    this.http.get<any>('/products/count').subscribe(({count}) => {
      this.count = count;
      this._count$.next(this.count);
    },
  _ => this._count$.next(0));
  }

  getMyProductsViaServer(id) {
    return this.http.get<{products: Product[]}>(`/products/myproducts/${id}`);
  }

  addProduct(product) {
    return this.http.post<any>('/products/add', product, {
      reportProgress: true,
      observe: 'events'
    });
  }


  getPageProducts(id): Observable<Product[]> | Product[] {
    if (this.products[id]) {
      return this.products[id];
    } else {
      return this.http.get<{products: Product[]}>(`/products/global/${id}/${this.count}`).pipe(
        map(({products}) => {
          this.products[`${id}`] = products;
          return products;
        }));
    }
  }

  removeProduct(productId) {} // TODO:

  toggleWishList(product) { // TODO:
    console.log(product);
  }


  updateCartAfterLogin(cart) { return this.http.post<any>('/cart/update', cart); }

  clearCart() {
    this.auth.auth$.pipe(take(1)).subscribe(isAuth => {
      if (!isAuth) {
        this.cart = [];
        this._cart$.next(this.cart);
        localStorage.setItem('cart', JSON.stringify(this.cart));
      } else {
        this.http.post<any>('/cart/clear', {
          _id: localStorage.getItem('_id')
        })
        .subscribe(
          ({cart}: {cart: Product[]}) => {
            this.cart = cart;
            localStorage.setItem('cart', JSON.stringify(this.cart));
            this._cart$.next(this.cart);
          },
          err => {}
        );
      }
    });
  }

  toggleCartById(id) {
    this.auth.auth$.pipe(take(1)).subscribe(isAuth => {
      let index = (this.cart || []).findIndex(product => product._id === id);
      if (index !== -1) {
        if (!isAuth) {
          this.cart.splice(index, 1);
          localStorage.setItem('cart', JSON.stringify(this.cart));
          this._cart$.next(this.cart);
          return;
        }
        return this.http.post('/cart/remove', {
          product: this.cart[index],
          _id: localStorage.getItem('_id')
        })
          .subscribe(
            ({cart}: {cart: Product[]}) => {
              this.cart = cart;
              localStorage.setItem('cart', JSON.stringify(this.cart));
              this._cart$.next(this.cart);
            },
            err => {}
          );
      }
      let myKey;
      for (const key in this.products) {
        if (1) {
          index = (this.products[key] || []).findIndex(product => product._id === id);
          if (index > -1) {
            myKey = key;
            break;
          }
        }
      }
      if (index > -1) {
        if (!isAuth) {
          this.cart.unshift(this.products[myKey][index]);
          localStorage.setItem('cart', JSON.stringify(this.cart));
          this._cart$.next(this.cart);
          return;
        }
        this.http.post('/cart/add', {
          product: this.products[myKey][index],
          _id: localStorage.getItem('_id')
        })
          .subscribe(
            ({cart}: {cart: Product[]}) => {
              this.cart = cart;
              localStorage.setItem('cart', JSON.stringify(this.cart));
              this._cart$.next(this.cart);
            },
            err => {}
          );
      }

    });
  }

}
