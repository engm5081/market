import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private _state$: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);
  state$: Observable<Boolean> = this._state$.asObservable();

  changeState(state) {
    this._state$.next(state);
  }

}
