export interface Product  {
  amount: Number;
  date: String;
  describtion: String;
  image: String;
  owner: String;
  ownerId: String;
  ownerImg: String;
  price: Number;
  _id: String;
  rate: Number;
  title: String;
}
