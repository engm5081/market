export interface SignupUser {
  email: String;
  password: String;
  firstName: String;
  gender: String;
  lastName: String;
}

export interface SigninUser {
  email: String;
  password: String;
}

export class User {
  constructor(
    public firstName: String,
    public lastName: String,
    public email: String,
    public gender: String,
    public token: String,
    public _id: String,
    public wishList: Array<any>, // TODO: edit any
    public cart: Array<any>, // TODO: edit any
    public expiresIn: Number,
    public date: String,
    public about?: String
  ) {}
}
