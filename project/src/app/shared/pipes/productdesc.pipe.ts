import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productdesc'
})
export class ProductdescPipe implements PipeTransform {

  transform(value: String, length: Number | String): String {
    if (value.length > +length) {
      return value.slice(0, +length) + ' ...';
    }
    return value;
  }

}
