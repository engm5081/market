const app = require('express')();
const { mongoUrl, port } = require("../configs/config");
const mongoose = require("mongoose");
mongoose.connect(mongoUrl, {useNewUrlParser: true}, () => {
    console.log('seeding db connected');
    productSeeding(info);
});
require("../models/product");
const Product = mongoose.model("product");

let x = 0;
function productSeeding(datas) {
    const data = {...datas[x]};
    data.ownerId = datas[x].ownerId.$oid;
    console.log(data);
    data.rate = Math.random() * 5;
    new Product(data)
    .save(() => {
        x++;
        if (x === datas.length) {
            console.log('done');
            x = 0;
            return;
        }
        productSeeding(datas);
    });
}

app.listen(port, () => console.log('server started!'));

const info = [{
    "title": "Soup - Verve - Chipotle Chicken",
    "price": 94,
    "amount": 162,
    "describtion": "Nondisplaced transverse fracture of shaft of humerus, right arm, subsequent encounter for fracture with delayed healing",
    "image": "http://dummyimage.com/255x262.jpg/dddddd/000000",
    "owner": "Aimil Tinner",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000000"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Chip - Potato Dill Pickle",
    "price": 73,
    "amount": 318,
    "describtion": "Underdosing of unspecified fibrinolysis-affecting drugs",
    "image": "http://dummyimage.com/266x254.jpg/ff4444/ffffff",
    "owner": "Quincey Lucchi",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000001"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Pepsi - Diet, 355 Ml",
    "price": 92,
    "amount": 207,
    "describtion": "Pain in unspecified knee",
    "image": "http://dummyimage.com/252x267.png/cc0000/ffffff",
    "owner": "Colly Simukov",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000002"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/dddddd/000000"
  }, {
    "title": "Goat - Whole Cut",
    "price": 79,
    "amount": 505,
    "describtion": "Superficial foreign body of left shoulder, sequela",
    "image": "http://dummyimage.com/263x251.bmp/5fa2dd/ffffff",
    "owner": "Erin Tweedie",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000003"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Lime Cordial - Roses",
    "price": 69,
    "amount": 757,
    "describtion": "Superficial thrombophlebitis in the puerperium",
    "image": "http://dummyimage.com/255x255.bmp/ff4444/ffffff",
    "owner": "Hailee Magnus",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000004"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Tea - Black Currant",
    "price": 97,
    "amount": 778,
    "describtion": "Injury of pulmonary blood vessels",
    "image": "http://dummyimage.com/257x260.bmp/ff4444/ffffff",
    "owner": "Saunderson Testin",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000005"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Salt And Pepper Mix - Black",
    "price": 81,
    "amount": 480,
    "describtion": "Displaced unspecified condyle fracture of lower end of right femur, subsequent encounter for open fracture type I or II with malunion",
    "image": "http://dummyimage.com/257x250.bmp/5fa2dd/ffffff",
    "owner": "Klemens Caig",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000006"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Tomatoes - Cherry, Yellow",
    "price": 70,
    "amount": 75,
    "describtion": "Optic nerve hypoplasia, left eye",
    "image": "http://dummyimage.com/258x250.jpg/cc0000/ffffff",
    "owner": "Hollyanne Pawels",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000007"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/dddddd/000000"
  }, {
    "title": "Cheese Cheddar Processed",
    "price": 92,
    "amount": 402,
    "describtion": "Subluxation of symphysis (pubis) in pregnancy, first trimester",
    "image": "http://dummyimage.com/262x257.jpg/ff4444/ffffff",
    "owner": "Zelig Skirlin",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000008"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Cognac - Courvaisier",
    "price": 89,
    "amount": 412,
    "describtion": "Puncture wound with foreign body of unspecified upper arm, subsequent encounter",
    "image": "http://dummyimage.com/267x252.bmp/5fa2dd/ffffff",
    "owner": "Seka Larwell",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000009"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Tamarillo",
    "price": 97,
    "amount": 664,
    "describtion": "Unspecified occupant of bus injured in collision with pedestrian or animal in nontraffic accident, sequela",
    "image": "http://dummyimage.com/259x270.jpg/dddddd/000000",
    "owner": "Rodrique Kilmurray",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500000a"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Cumin - Ground",
    "price": 80,
    "amount": 977,
    "describtion": "Unspecified injury of unspecified blood vessel at ankle and foot level, right leg, subsequent encounter",
    "image": "http://dummyimage.com/255x265.png/dddddd/000000",
    "owner": "Elberta Auckland",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500000b"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Glass - Wine, Plastic, Clear 5 Oz",
    "price": 115,
    "amount": 525,
    "describtion": "Other sphingolipidosis",
    "image": "http://dummyimage.com/256x258.bmp/cc0000/ffffff",
    "owner": "Stormi Petroff",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500000c"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Rice - Jasmine Sented",
    "price": 113,
    "amount": 379,
    "describtion": "Unspecified injury of axillary or brachial vein, unspecified side",
    "image": "http://dummyimage.com/264x260.png/5fa2dd/ffffff",
    "owner": "Broddie Flitcroft",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500000d"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Quiche Assorted",
    "price": 69,
    "amount": 570,
    "describtion": "Nondisplaced fracture of hook process of hamate [unciform] bone, unspecified wrist, subsequent encounter for fracture with delayed healing",
    "image": "http://dummyimage.com/258x265.jpg/cc0000/ffffff",
    "owner": "Binni Ellerbeck",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500000e"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Sherbet - Raspberry",
    "price": 112,
    "amount": 255,
    "describtion": "Stress fracture, other site, subsequent encounter for fracture with malunion",
    "image": "http://dummyimage.com/262x268.jpg/dddddd/000000",
    "owner": "Rorie Cashley",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500000f"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Nori Sea Weed",
    "price": 72,
    "amount": 968,
    "describtion": "Maternal care for intrauterine death, fetus 4",
    "image": "http://dummyimage.com/258x270.jpg/5fa2dd/ffffff",
    "owner": "Wit Guirau",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000010"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/cc0000/ffffff"
  }, {
    "title": "Turkey - Breast, Boneless Sk On",
    "price": 103,
    "amount": 357,
    "describtion": "Torus fracture of lower end of right tibia, subsequent encounter for fracture with nonunion",
    "image": "http://dummyimage.com/270x267.bmp/ff4444/ffffff",
    "owner": "Tina Hirth",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000011"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/dddddd/000000"
  }, {
    "title": "Sweet Pea Sprouts",
    "price": 93,
    "amount": 37,
    "describtion": "Burn of unspecified degree of single right finger (nail) except thumb, initial encounter",
    "image": "http://dummyimage.com/254x259.bmp/ff4444/ffffff",
    "owner": "Catherine Schinetti",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000012"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Banana - Leaves",
    "price": 109,
    "amount": 960,
    "describtion": "Poisoning by other parasympathomimetics [cholinergics], intentional self-harm, subsequent encounter",
    "image": "http://dummyimage.com/266x270.jpg/cc0000/ffffff",
    "owner": "Aubrey Kearle",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000013"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Appetizer - Mini Egg Roll, Shrimp",
    "price": 86,
    "amount": 183,
    "describtion": "Poisoning by other topical agents, assault, sequela",
    "image": "http://dummyimage.com/256x259.png/dddddd/000000",
    "owner": "Suellen Heijnen",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000014"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Flour - All Purpose",
    "price": 36,
    "amount": 369,
    "describtion": "Laceration with foreign body of abdominal wall, left upper quadrant with penetration into peritoneal cavity, sequela",
    "image": "http://dummyimage.com/264x255.png/dddddd/000000",
    "owner": "Ricardo Cooper",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000015"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/cc0000/ffffff"
  }, {
    "title": "Tea Leaves - Oolong",
    "price": 48,
    "amount": 918,
    "describtion": "Diffuse interstitial keratitis",
    "image": "http://dummyimage.com/260x253.bmp/ff4444/ffffff",
    "owner": "Ediva Dabes",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000016"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Herb Du Provence - Primerba",
    "price": 100,
    "amount": 940,
    "describtion": "Displaced fracture of medial cuneiform of right foot, subsequent encounter for fracture with nonunion",
    "image": "http://dummyimage.com/269x254.bmp/cc0000/ffffff",
    "owner": "Shantee Janku",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000017"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Split Peas - Yellow, Dry",
    "price": 35,
    "amount": 423,
    "describtion": "Chronic venous hypertension (idiopathic) with other complications of bilateral lower extremity",
    "image": "http://dummyimage.com/261x256.jpg/dddddd/000000",
    "owner": "Tiffanie Paramore",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000018"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Chocolate - Semi Sweet, Calets",
    "price": 93,
    "amount": 793,
    "describtion": "Toxic effect of contact with other venomous marine animals",
    "image": "http://dummyimage.com/258x257.bmp/dddddd/000000",
    "owner": "Imogen Wigfield",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000019"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Chocolate - Chips Compound",
    "price": 46,
    "amount": 121,
    "describtion": "Benign neoplasm of other specified male genital organs",
    "image": "http://dummyimage.com/264x263.bmp/ff4444/ffffff",
    "owner": "Forest Gland",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500001a"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/cc0000/ffffff"
  }, {
    "title": "Veal - Loin",
    "price": 50,
    "amount": 961,
    "describtion": "Nondisplaced fracture of trapezium [larger multangular], left wrist, initial encounter for closed fracture",
    "image": "http://dummyimage.com/251x264.png/ff4444/ffffff",
    "owner": "Evelyn Skelly",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500001b"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Lettuce - Romaine",
    "price": 39,
    "amount": 511,
    "describtion": "Drug-induced interstitial lung disorders, unspecified",
    "image": "http://dummyimage.com/251x252.bmp/cc0000/ffffff",
    "owner": "Adrian Ivell",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500001c"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Scallops - 20/30",
    "price": 84,
    "amount": 30,
    "describtion": "Other specified injury of radial artery at forearm level, left arm",
    "image": "http://dummyimage.com/260x259.png/cc0000/ffffff",
    "owner": "Orella Crumly",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500001d"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Gelatine Leaves - Bulk",
    "price": 94,
    "amount": 635,
    "describtion": "Breakdown (mechanical) of implanted electronic neurostimulator, generator, subsequent encounter",
    "image": "http://dummyimage.com/263x260.jpg/dddddd/000000",
    "owner": "Theodore Faherty",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500001e"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/cc0000/ffffff"
  }, {
    "title": "Potatoes - Idaho 100 Count",
    "price": 94,
    "amount": 462,
    "describtion": "Fracture of unspecified phalanx of left little finger, initial encounter for open fracture",
    "image": "http://dummyimage.com/264x258.png/dddddd/000000",
    "owner": "Arlen Cadden",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500001f"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Sorrel - Fresh",
    "price": 90,
    "amount": 401,
    "describtion": "External constriction of left index finger",
    "image": "http://dummyimage.com/250x257.jpg/cc0000/ffffff",
    "owner": "Irving Chell",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000020"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Bar - Sweet And Salty Chocolate",
    "price": 89,
    "amount": 52,
    "describtion": "Rheumatoid arthritis of left wrist with involvement of other organs and systems",
    "image": "http://dummyimage.com/266x270.bmp/ff4444/ffffff",
    "owner": "Joye Fabler",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000021"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Vacuum Bags 12x16",
    "price": 79,
    "amount": 256,
    "describtion": "Burn of third degree of multiple sites of right shoulder and upper limb, except wrist and hand, initial encounter",
    "image": "http://dummyimage.com/257x267.jpg/dddddd/000000",
    "owner": "Russell Burder",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000022"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Wine - Shiraz South Eastern",
    "price": 35,
    "amount": 173,
    "describtion": "Laceration of muscle, fascia and tendon of unspecified hip, sequela",
    "image": "http://dummyimage.com/256x256.bmp/dddddd/000000",
    "owner": "Dallon Matteacci",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000023"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Lemon Pepper",
    "price": 53,
    "amount": 305,
    "describtion": "Other pulmonary collapse",
    "image": "http://dummyimage.com/258x258.png/ff4444/ffffff",
    "owner": "Petra Bonwick",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000024"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/ff4444/ffffff"
  }, {
    "title": "Sauce - Cranberry",
    "price": 81,
    "amount": 505,
    "describtion": "Displaced bicondylar fracture of right tibia, subsequent encounter for closed fracture with routine healing",
    "image": "http://dummyimage.com/265x251.jpg/dddddd/000000",
    "owner": "Anthea Artingstall",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000025"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Oregano - Dry, Rubbed",
    "price": 37,
    "amount": 508,
    "describtion": "Stable burst fracture of fourth lumbar vertebra, sequela",
    "image": "http://dummyimage.com/256x265.png/ff4444/ffffff",
    "owner": "Nita Lumsdale",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000026"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Rabbit - Saddles",
    "price": 70,
    "amount": 137,
    "describtion": "Contusion of fallopian tube, unspecified",
    "image": "http://dummyimage.com/259x255.bmp/5fa2dd/ffffff",
    "owner": "See Brandreth",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000027"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Bagel - Sesame Seed Presliced",
    "price": 30,
    "amount": 372,
    "describtion": "Toxic effect of chlorine gas, assault",
    "image": "http://dummyimage.com/264x258.bmp/ff4444/ffffff",
    "owner": "Megen Creeghan",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000028"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/cc0000/ffffff"
  }, {
    "title": "Wine - Vineland Estate Semi - Dry",
    "price": 67,
    "amount": 71,
    "describtion": "Hang-glider crash injuring occupant",
    "image": "http://dummyimage.com/251x259.bmp/cc0000/ffffff",
    "owner": "Reggy Standring",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000029"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Quail Eggs - Canned",
    "price": 97,
    "amount": 594,
    "describtion": "Hit or struck by falling object due to accident to passenger ship, subsequent encounter",
    "image": "http://dummyimage.com/256x259.bmp/cc0000/ffffff",
    "owner": "Walliw Robelet",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500002a"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Dooleys Toffee",
    "price": 69,
    "amount": 544,
    "describtion": "Dislocation of left acromioclavicular joint, 100%-200% displacement",
    "image": "http://dummyimage.com/256x270.bmp/cc0000/ffffff",
    "owner": "Belia Greenhill",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500002b"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Foie Gras",
    "price": 107,
    "amount": 103,
    "describtion": "Diverticulitis of large intestine without perforation or abscess with bleeding",
    "image": "http://dummyimage.com/269x261.png/ff4444/ffffff",
    "owner": "Rosella Mapples",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500002c"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Praline Paste",
    "price": 105,
    "amount": 457,
    "describtion": "Other schistosomiasis",
    "image": "http://dummyimage.com/257x258.jpg/cc0000/ffffff",
    "owner": "Aretha Radford",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500002d"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Tart Shells - Savory, 4",
    "price": 41,
    "amount": 780,
    "describtion": "Accidental discharge of machine gun",
    "image": "http://dummyimage.com/264x265.bmp/5fa2dd/ffffff",
    "owner": "Vincent Chaucer",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500002e"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Coffee - Irish Cream",
    "price": 83,
    "amount": 739,
    "describtion": "Adrenogenital disorders",
    "image": "http://dummyimage.com/267x250.bmp/cc0000/ffffff",
    "owner": "Lilith Scarrott",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500002f"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Samosa - Veg",
    "price": 100,
    "amount": 129,
    "describtion": "Toxic effect of alcohol",
    "image": "http://dummyimage.com/251x253.jpg/dddddd/000000",
    "owner": "Neville Steadman",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000030"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/dddddd/000000"
  }, {
    "title": "Veal - Insides Provini",
    "price": 110,
    "amount": 628,
    "describtion": "Blister (nonthermal) of left ear",
    "image": "http://dummyimage.com/265x267.png/cc0000/ffffff",
    "owner": "Ban Crotty",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000031"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Stock - Veal, White",
    "price": 80,
    "amount": 183,
    "describtion": "Lennox-Gastaut syndrome, intractable, without status epilepticus",
    "image": "http://dummyimage.com/262x266.jpg/dddddd/000000",
    "owner": "Raddie Castell",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000032"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Cheese - Cheddar, Old White",
    "price": 67,
    "amount": 82,
    "describtion": "Nondisplaced fracture of proximal phalanx of right index finger, subsequent encounter for fracture with delayed healing",
    "image": "http://dummyimage.com/255x250.jpg/cc0000/ffffff",
    "owner": "Lorie Sooper",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000033"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/dddddd/000000"
  }, {
    "title": "Cocktail Napkin Blue",
    "price": 116,
    "amount": 343,
    "describtion": "Intentional self-harm by other specified means, initial encounter",
    "image": "http://dummyimage.com/252x262.png/ff4444/ffffff",
    "owner": "Odilia Winwright",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000034"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Juice - Pineapple, 341 Ml",
    "price": 73,
    "amount": 964,
    "describtion": "Traumatic hemorrhage of cerebrum, unspecified, with loss of consciousness greater than 24 hours without return to pre-existing conscious level with patient surviving, sequela",
    "image": "http://dummyimage.com/250x250.bmp/dddddd/000000",
    "owner": "Stepha Bayle",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000035"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Beans - Black Bean, Preserved",
    "price": 92,
    "amount": 217,
    "describtion": "Other osteoporosis with current pathological fracture, left femur, initial encounter for fracture",
    "image": "http://dummyimage.com/264x261.bmp/ff4444/ffffff",
    "owner": "Lisa Tissiman",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000036"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Apple - Macintosh",
    "price": 70,
    "amount": 149,
    "describtion": "Fracture of base of skull, right side, subsequent encounter for fracture with delayed healing",
    "image": "http://dummyimage.com/262x258.png/cc0000/ffffff",
    "owner": "Clement Lumsdall",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000037"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/dddddd/000000"
  }, {
    "title": "Everfresh Products",
    "price": 32,
    "amount": 97,
    "describtion": "Deprivation amblyopia, unspecified eye",
    "image": "http://dummyimage.com/269x259.bmp/dddddd/000000",
    "owner": "Marwin Fasham",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000038"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Buffalo - Short Rib Fresh",
    "price": 79,
    "amount": 516,
    "describtion": "Salter-Harris Type IV physeal fracture of lower end of right tibia, subsequent encounter for fracture with delayed healing",
    "image": "http://dummyimage.com/265x251.bmp/dddddd/000000",
    "owner": "Dulciana Chesterton",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000039"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Wine - Jaboulet Cotes Du Rhone",
    "price": 98,
    "amount": 681,
    "describtion": "Glaucoma secondary to eye inflammation, right eye, moderate stage",
    "image": "http://dummyimage.com/256x265.png/ff4444/ffffff",
    "owner": "Adelheid Dumsday",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500003a"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/ff4444/ffffff"
  }, {
    "title": "Pasta - Fett Alfredo, Single Serve",
    "price": 50,
    "amount": 148,
    "describtion": "Puncture wound with foreign body of left little finger without damage to nail, initial encounter",
    "image": "http://dummyimage.com/266x264.png/dddddd/000000",
    "owner": "Ferguson Whipple",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500003b"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Bread Crumbs - Japanese Style",
    "price": 89,
    "amount": 454,
    "describtion": "Niemann-Pick disease type A",
    "image": "http://dummyimage.com/258x252.png/5fa2dd/ffffff",
    "owner": "Demetria Caudell",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500003c"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Sun - Dried Tomatoes",
    "price": 84,
    "amount": 143,
    "describtion": "Minor laceration of abdominal aorta, initial encounter",
    "image": "http://dummyimage.com/269x263.bmp/5fa2dd/ffffff",
    "owner": "Rhodie Nyles",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500003d"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Beef - Short Ribs",
    "price": 104,
    "amount": 578,
    "describtion": "Nondisplaced fracture of epiphysis (separation) (upper) of right femur, subsequent encounter for closed fracture with nonunion",
    "image": "http://dummyimage.com/258x253.bmp/dddddd/000000",
    "owner": "Xever Lapidus",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500003e"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Gherkin - Sour",
    "price": 76,
    "amount": 291,
    "describtion": "Incomplete atypical femoral fracture, left leg, subsequent encounter for fracture with malunion",
    "image": "http://dummyimage.com/265x264.bmp/cc0000/ffffff",
    "owner": "Koren Arthan",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500003f"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Eggplant Italian",
    "price": 75,
    "amount": 71,
    "describtion": "Congenital malformations of uterus and cervix",
    "image": "http://dummyimage.com/267x262.jpg/5fa2dd/ffffff",
    "owner": "Kirstyn Lemonby",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000040"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Pork Ham Prager",
    "price": 105,
    "amount": 719,
    "describtion": "Contact with other hot objects, undetermined intent, initial encounter",
    "image": "http://dummyimage.com/266x251.jpg/cc0000/ffffff",
    "owner": "Ally Glass",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000041"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Chocolate Bar - Coffee Crisp",
    "price": 49,
    "amount": 94,
    "describtion": "Tear of articular cartilage of left knee, current, initial encounter",
    "image": "http://dummyimage.com/252x262.bmp/5fa2dd/ffffff",
    "owner": "Marie-ann Devinn",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000042"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Lamb Rack Frenched Australian",
    "price": 73,
    "amount": 668,
    "describtion": "Disease of gallbladder, unspecified",
    "image": "http://dummyimage.com/263x251.bmp/5fa2dd/ffffff",
    "owner": "Car Richardes",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000043"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Sour Puss Sour Apple",
    "price": 44,
    "amount": 198,
    "describtion": "Motorcycle driver injured in collision with pedestrian or animal in nontraffic accident, sequela",
    "image": "http://dummyimage.com/259x262.bmp/5fa2dd/ffffff",
    "owner": "Clementina Rounds",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000044"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Mushroom - White Button",
    "price": 84,
    "amount": 122,
    "describtion": "Strain of muscle(s) and tendon(s) of the rotator cuff of left shoulder, subsequent encounter",
    "image": "http://dummyimage.com/266x255.jpg/ff4444/ffffff",
    "owner": "Clem Jacobowitz",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000045"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Tofu - Soft",
    "price": 91,
    "amount": 503,
    "describtion": "Other superficial bite of left lesser toe(s), initial encounter",
    "image": "http://dummyimage.com/260x267.png/5fa2dd/ffffff",
    "owner": "Bran Czyz",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000046"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Star Fruit",
    "price": 51,
    "amount": 433,
    "describtion": "Other injury of liver, sequela",
    "image": "http://dummyimage.com/260x257.png/5fa2dd/ffffff",
    "owner": "Dix Venus",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000047"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Beans - Black Bean, Preserved",
    "price": 77,
    "amount": 887,
    "describtion": "Partial traumatic amputation of right foot, level unspecified, sequela",
    "image": "http://dummyimage.com/258x252.png/cc0000/ffffff",
    "owner": "Rochell Gladeche",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000048"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/ff4444/ffffff"
  }, {
    "title": "Macaroons - Two Bite Choc",
    "price": 94,
    "amount": 337,
    "describtion": "Unspecified open wound of right thumb with damage to nail, subsequent encounter",
    "image": "http://dummyimage.com/262x252.png/cc0000/ffffff",
    "owner": "Prudy Bulmer",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000049"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Stock - Beef, White",
    "price": 79,
    "amount": 368,
    "describtion": "Laceration of intrinsic muscle, fascia and tendon of left middle finger at wrist and hand level, subsequent encounter",
    "image": "http://dummyimage.com/268x269.png/dddddd/000000",
    "owner": "Silvain Tran",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500004a"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Mustard - Dijon",
    "price": 47,
    "amount": 539,
    "describtion": "Bronchopneumonia, unspecified organism",
    "image": "http://dummyimage.com/270x267.bmp/5fa2dd/ffffff",
    "owner": "Rodina Hoy",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500004b"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/cc0000/ffffff"
  }, {
    "title": "Trout - Rainbow, Fresh",
    "price": 96,
    "amount": 398,
    "describtion": "Nontraumatic ischemic infarction of muscle, right thigh",
    "image": "http://dummyimage.com/251x261.bmp/5fa2dd/ffffff",
    "owner": "Van Clynman",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500004c"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Water - Aquafina Vitamin",
    "price": 93,
    "amount": 522,
    "describtion": "Unspecified traumatic cataract, unspecified eye",
    "image": "http://dummyimage.com/268x257.jpg/5fa2dd/ffffff",
    "owner": "Zola Aynsley",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500004d"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/ff4444/ffffff"
  }, {
    "title": "Broom - Angled",
    "price": 74,
    "amount": 81,
    "describtion": "Malignant melanoma of right eyelid, including canthus",
    "image": "http://dummyimage.com/255x266.bmp/dddddd/000000",
    "owner": "Kristina Garnul",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500004e"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/5fa2dd/ffffff"
  }, {
    "title": "Nut - Pecan, Pieces",
    "price": 38,
    "amount": 63,
    "describtion": "Breakdown (mechanical) of heart valve prosthesis, subsequent encounter",
    "image": "http://dummyimage.com/259x255.jpg/cc0000/ffffff",
    "owner": "Ursa Lelievre",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500004f"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/dddddd/000000"
  }, {
    "title": "Pineapple - Golden",
    "price": 95,
    "amount": 733,
    "describtion": "Pedestrian with other conveyance injured in collision with heavy transport vehicle or bus in nontraffic accident, initial encounter",
    "image": "http://dummyimage.com/268x263.png/5fa2dd/ffffff",
    "owner": "Alverta Antoinet",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000050"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/5fa2dd/ffffff"
  }, {
    "title": "Muffins - Assorted",
    "price": 51,
    "amount": 867,
    "describtion": "Contact with powered lawn mower, sequela",
    "image": "http://dummyimage.com/257x258.jpg/dddddd/000000",
    "owner": "Ado Dunkerton",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000051"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Bread - Pullman, Sliced",
    "price": 35,
    "amount": 9,
    "describtion": "Poisoning by other hormone antagonists, undetermined, sequela",
    "image": "http://dummyimage.com/267x254.bmp/cc0000/ffffff",
    "owner": "Sisile Deroche",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000052"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Oven Mitts - 15 Inch",
    "price": 99,
    "amount": 7,
    "describtion": "Strain of intrinsic muscle, fascia and tendon of right index finger at wrist and hand level, initial encounter",
    "image": "http://dummyimage.com/251x256.jpg/dddddd/000000",
    "owner": "Chas Tegeller",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000053"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Wine - White, Chardonnay",
    "price": 63,
    "amount": 634,
    "describtion": "Pathological fracture, left ulna, initial encounter for fracture",
    "image": "http://dummyimage.com/256x269.png/cc0000/ffffff",
    "owner": "Paulo Smissen",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000054"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Lamb Tenderloin Nz Fr",
    "price": 33,
    "amount": 537,
    "describtion": "Exposure to tanning bed",
    "image": "http://dummyimage.com/260x254.jpg/5fa2dd/ffffff",
    "owner": "Arlina Lancashire",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000055"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Beans - Kidney White",
    "price": 46,
    "amount": 184,
    "describtion": "Keratoconjunctivitis sicca, not specified as Sjogren's, left eye",
    "image": "http://dummyimage.com/262x264.bmp/5fa2dd/ffffff",
    "owner": "Dynah Jell",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000056"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Table Cloth 144x90 White",
    "price": 81,
    "amount": 589,
    "describtion": "Laceration with foreign body of unspecified part of head, subsequent encounter",
    "image": "http://dummyimage.com/269x268.bmp/5fa2dd/ffffff",
    "owner": "Temple Fattore",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000057"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Tortillas - Flour, 12",
    "price": 88,
    "amount": 623,
    "describtion": "3-part fracture of surgical neck of unspecified humerus, initial encounter for open fracture",
    "image": "http://dummyimage.com/264x264.bmp/5fa2dd/ffffff",
    "owner": "Bamby Hammett",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000058"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/dddddd/000000"
  }, {
    "title": "Mushroom - Shitake, Dry",
    "price": 57,
    "amount": 336,
    "describtion": "Legal intervention involving injury by machine gun, bystander injured",
    "image": "http://dummyimage.com/261x261.bmp/ff4444/ffffff",
    "owner": "Derick Tomankowski",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000059"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/5fa2dd/ffffff"
  }, {
    "title": "Spring Roll Veg Mini",
    "price": 32,
    "amount": 590,
    "describtion": "Complete traumatic transphalangeal amputation of unspecified thumb, subsequent encounter",
    "image": "http://dummyimage.com/251x261.png/cc0000/ffffff",
    "owner": "Cassey Crock",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500005a"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/dddddd/000000"
  }, {
    "title": "Plums - Red",
    "price": 109,
    "amount": 683,
    "describtion": "Nondisplaced fracture of distal phalanx of left middle finger",
    "image": "http://dummyimage.com/250x257.jpg/5fa2dd/ffffff",
    "owner": "Ainslie Blackshaw",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500005b"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Otomegusa Dashi Konbu",
    "price": 70,
    "amount": 43,
    "describtion": "Displaced supracondylar fracture with intracondylar extension of lower end of right femur, subsequent encounter for open fracture type I or II with malunion",
    "image": "http://dummyimage.com/251x253.bmp/ff4444/ffffff",
    "owner": "Tova Gouny",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500005c"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/dddddd/000000"
  }, {
    "title": "Melon - Watermelon, Seedless",
    "price": 76,
    "amount": 143,
    "describtion": "Anterior subluxation of unspecified sternoclavicular joint, initial encounter",
    "image": "http://dummyimage.com/256x260.jpg/dddddd/000000",
    "owner": "Everard Lovell",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500005d"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Mushroom - Enoki, Dry",
    "price": 117,
    "amount": 652,
    "describtion": "Cholera due to Vibrio cholerae 01, biovar eltor",
    "image": "http://dummyimage.com/260x253.bmp/5fa2dd/ffffff",
    "owner": "Doe Vannikov",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500005e"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/dddddd/000000"
  }, {
    "title": "Sausage - Andouille",
    "price": 48,
    "amount": 792,
    "describtion": "Malignant neoplasm of external lower lip",
    "image": "http://dummyimage.com/257x270.jpg/ff4444/ffffff",
    "owner": "Ferguson Humphery",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba500005f"
    },
    "ownerImg": "http://dummyimage.com/150x150.bmp/ff4444/ffffff"
  }, {
    "title": "Wiberg Super Cure",
    "price": 89,
    "amount": 35,
    "describtion": "Effusion, hand",
    "image": "http://dummyimage.com/258x257.bmp/5fa2dd/ffffff",
    "owner": "Allianora Mallard",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000060"
    },
    "ownerImg": "http://dummyimage.com/150x150.jpg/cc0000/ffffff"
  }, {
    "title": "Kaffir Lime Leaves",
    "price": 91,
    "amount": 366,
    "describtion": "Laceration of muscle, fascia and tendon at neck level, subsequent encounter",
    "image": "http://dummyimage.com/268x251.jpg/ff4444/ffffff",
    "owner": "Becky Nezey",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000061"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/ff4444/ffffff"
  }, {
    "title": "Tortillas - Flour, 8",
    "price": 39,
    "amount": 690,
    "describtion": "Necrotizing enterocolitis, unspecified",
    "image": "http://dummyimage.com/252x251.bmp/5fa2dd/ffffff",
    "owner": "Darleen Blackah",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000062"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/cc0000/ffffff"
  }, {
    "title": "Nut - Chestnuts, Whole",
    "price": 77,
    "amount": 129,
    "describtion": "Dissociative amnesia",
    "image": "http://dummyimage.com/262x265.bmp/ff4444/ffffff",
    "owner": "Celestina Perott",
    "ownerId": {
      "$oid": "5ba381c7fc13ae7ba5000063"
    },
    "ownerImg": "http://dummyimage.com/150x150.png/ff4444/ffffff"
  }];