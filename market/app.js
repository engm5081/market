const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const validator = require('express-validator');
const app = express();
const { port, mongoUrl } = require("./configs/config");

// connect to database
mongoose.connect(mongoUrl, {useNewUrlParser: true}, () => console.log(`DB Connected on ${mongoUrl}`));
require("./models/user");
require("./models/product");

// set middle wares
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(validator());

// set app routes
const main = require("./routes/main");
const user = require("./routes/user");
const products = require("./routes/products");
const cart = require("./routes/cart");
app.use("/", main);
app.use("/user", user);
app.use("/products", products);
app.use("/cart", cart);

app.listen(port, () => console.log(`server started on port: ${port}`));