const multer = require("multer");
const fs = require('fs');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const dir = 'public/assets/productsImages/'; 
        fs.mkdir(dir, _ => {
            cb(null, dir);
        });
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/png'  ||
        file.mimetype === 'image/jpg') {
            cb(null, true);
        } else {
            cb(null, false);
        }
};

const upload = multer({
    storage, 
    limits: {
        fieldSize: 1024 * 1024 * 5
    },
    fileFilter
});

module.exports = upload;