const fs = require("fs");
const helpers = {};

helpers.removeFile = (path) => {
    fs.unlink(path, (err) => {
        if (err) throw err;
    });
};

helpers.removeDuplicates = (myArr, prop) => {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
}

module.exports = helpers;