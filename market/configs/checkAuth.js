const jwt = require("jsonwebtoken");
const { jwtSecret } = require("./config");

const checkAuth = (req, res, next) => {
    const token = req.headers['authorization'];
    try {
        jwt.verify(token, jwtSecret);
        next();
    } catch (_) {
        res.status(401).json({
            errors: ["Please login to continue"]
        });
    }
};

module.exports = checkAuth;