const router = require("express").Router();
const checkAuth = require("../configs/checkAuth");
const mongoose = require("mongoose");
const upload = require("../configs/multer");
const { removeFile } = require("../configs/helpers");
const User = mongoose.model("user");
const Product = mongoose.model("product");

router.post("/", (req, res) => {
    console.log(req.body); // **
});


router.use(checkAuth);


router.post("/add", upload.single('image'),(req, res) => {
    const errors = [];
    
    const { path } = req.file || {};

    if (!path) {
        errors.push("Product image is required");
    }

    let { product, user } = req.body || {"product": "{}", "user": "{}"};
    product = JSON.parse(product);
    user = JSON.parse(user);
    if (!product.title) {
        errors.push("Product name is required");
    } else {
        if (product.title.length < 2) errors.push("Product name min length is 2");
    }

    if (!product.describtion) {
        errors.push("Product describtion is required");
    } else {
        if (product.describtion.length < 50) errors.push("Product describtion min length is 50");
    }

    if (!product.price) {
        errors.push("Product price is required");
    } else {
        try {
            parseFloat(product.price);
        } catch(_) {
            errors.push("Product price must be a number");
        }
    }

    if (!product.price) {
        errors.push("Product price is required");
    } else {
        try {
            parseInt(product.price);
        } catch(_) {
            errors.push("Product price must be a number");
        }
    }

    if (!product.amount) {
        errors.push("Product amount is required");
    } else {
        try {
            parseInt(product.amount);
        } catch(_) {
            errors.push("Product amount must be a number");
        }
    }

    if (!user._id || !mongoose.Types.ObjectId.isValid(user._id)) {
        errors.push("Invalid user");
    }

    if (errors.length > 0) {
        removeFile(path);
        return res.status(400).json({
            errors
        });
    }
    const { _id } = user;
    User.findById(_id, "firstName lastName image", (err, user) => {
        if (err) {
            removeFile(path);
            return res.status(500).json({
                errors: ["Something went wrong"]
            });
        }
        if (!user) {
            removeFile(path);
            return res.status(401).json({
                errors: ["Invalid user"]
            });
        }

        new Product({
            ...product,
            image: path.slice(path.indexOf("/") + 1),
            owner: `${user.firstName} ${user.lastName}`,
            ownerId: _id,
            ownerImg: user.image
        })
        .save((err, product) => {
            if (err) {
                removeFile(path);
                return res.status(500).json({
                    errors: ["Something went wrong"]
                });
            }
            return res.json({
                product,
                msg: ["Successfully created product"]
            });
        });
    });
});

router.post("/remove", (req, res) => {
    
    res.json(true);
});

router.post("/togglewishlist", (req, res) => {
    res.json(true);

});

router.post("/togglecart", (req, res) => {
    res.json(true);
});

module.exports = router;