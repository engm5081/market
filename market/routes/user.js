const router = require("express").Router();
const mongoose = require("mongoose");
const User = mongoose.model("user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { jwtSecret } = require("../configs/config");
const { upload } = require("../configs/multer");

// signup new user
router.post("/signup", (req, res) => {
    req.checkBody("firstName", "First name min length is 2").trim().len({min: 2})
    req.checkBody("firstName", "First name is required").trim().notEmpty();
    req.checkBody("lastName", "Last name min length is 2").trim().len({min: 2});
    req.checkBody("lastName", "Last name is required").trim().notEmpty();
    req.checkBody("email", "Invalid email adress").trim().isEmail();
    req.checkBody("email", "Email address is required").trim().notEmpty();
    req.checkBody("password", "Password min length is 6").trim().len({min: 6});
    req.checkBody("password", "Password is required").trim().notEmpty();;
    let errors = req.validationErrors() || [];
    const userData = {...req.body};
    if (errors.length > 0) {
        errors = errors.map(err => {
            console.log(err); // **
            return err.msg;
        });
        if (userData.gender !== "male" && userData.gender !== "female") errors.push("Invalid gender");
        return res.status(400).json({ user: userData, errors });
    }
    User.findOne({email: userData.email}, (err, isUser) => {
        if (isUser) return res.status(400).json({ user: {...userData, email: ""}, errors: ["User already found"] });
        if (err) return res.status(500).json({ user: userData, errors: ["Something went wrong"] });
        userData.password = bcrypt.hashSync(userData.password, 10);
        new User(userData)
            .save()
            .then(user => {
                res.json({email: user.email}); // Something went wrong
            })
            .catch(_ => {
                return res.status(500).json({ user: userData, errors: ["Something went wrong"] });
            })
    });
});

// signin new user
router.post("/signin", (req, res) => {
    const { email, password } = req.body;
    User.findOne({email} ,(err, user) => {
        if (err) return res.status(500).json({errors: ["Something went wrong"]});
        if (!user) return res.status(400).json({errors: ["Invalid email or password"]});
        bcrypt.compare(password, user.password, (error, isMatch) => {
            if (error) return res.status(500).json({errors: ["Something went wrong"]});
            if (!isMatch) return res.status(400).json({errors: ["Invalid email or password"]});
            const token = jwt.sign({_id: user._id, date: new Date()}, jwtSecret, {
                expiresIn: 7200
            });
            res.json({
                _id: user._id,
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName,
                wishList: user.wishList,
                gender: user.gender,
                cart: user.cart,
                token,
                date: user.date,
                expiresIn: new Date().getTime() + 7200 * 1000
            });
        });
    });

});


router.post("/getById", (req, res) => {
    const { id } = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            errors: ["Invalid data"]
        });
    }
    User.findById(id, (err, user) => {
        if (err) return res.status(500).json({
            errors: ["Something went wrong"]
        });
        if (!user) return res.status(400).json({
            errors: ["Invalid data"]
        });
        res.json({user});
    });
});


module.exports = router;