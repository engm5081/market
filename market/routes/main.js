const router = require("express").Router();
const mongoose = require("mongoose");
const Product = mongoose.model("product");
const User = mongoose.model("user");
const path = require("path");
const { jwtSecret } = require("../configs/config");
const jwt = require("jsonwebtoken");


router.get("*", (req, res, next) => {
    if (req.path === "/products/count" ||
        req.path.match(/\/products\/myproducts\/\w+/) ||
        req.path.match(/\/products\/global\/\w+\/\w+/) ||
        req.path.match(/\/token\/refresh\/\w+/)) {
        console.log('test'); // **
        next();
    } else {
        res.sendFile(path.join(__dirname, "../public/index.html"));
    }
});


router.get("/products/count", (_, res) => {
    Product
        .count((err, count) => {
            if (err) return res.status(500).json({
                errors: ["Something went Wrong"]
            });
            res.json({count});
        });
});

router.get("/products/myproducts/:id", (req, res) => {
    const { id } = req.params;
    if (mongoose.Types.ObjectId.isValid(id)) {
        Product.find({ownerId: id}, (err, products) => {
            if (err) return res.json({products: []});
            res.json({products});
        });
    } else {
        res.json({products: []});
    }
});


router.get("/products/global/:id/:c", (req, res) => {
    const { id, c } = req.params;
    if (+id < 1) return res.json({
        errors: ["Invalid page number"]
    });
    const skip = (+id * 12) > +c ? 0 : +c - (+id * 12);
    Product.find({}, null, {limit: 12, skip: +skip}, (err, products) => {
                if (err) return res.json({products: []});
                res.json({products});
            });

});

router.get("/token/refresh/:id", (req, res) => {
    const { id } = req.params;
    if (mongoose.Types.ObjectId.isValid(id)) {
        User.findById(id, (err, user) => {
            if (err) return res.status(500).json({
                errors: ["Something went wrong"]
            });
            if (!user) return res.status(401).json({
                errors: ["Invalid account data"]
            });
            const token = jwt.sign({_id: id, date: new Date().toISOString()}, jwtSecret, {
                expiresIn: 7200
            });
            res.json({
                token,
                expiresIn: new Date().getTime() + 7200 * 1000
            })
        });
    } else {
        res.status(401).json({
            errors: ["Invalid account data"]
        });
    }
});

module.exports = router;
