const router = require("express").Router();
const mongoose = require("mongoose");
const User = mongoose.model("user");
const checkAuth = require("../configs/checkAuth");
const { removeDuplicates } = require("../configs/helpers");

router.use(checkAuth);

router.post("/clear", (req, res) => {
    const { _id } = req.body;
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(401).json({
        errors: ["Invalid account data"]
    });
    User.findById(_id, (err, user) => {
        if (err) return res.status(500).json({
            errors: ["Something went wrong"]
        });
        if (!user) return res.status(401).json({
            errors: ["Invalid account data"]
        });        
        user.cart = [];
        user.save((err2, newUser) => {
            if (err2) return res.status(500).json({
                errors: ["Something went wrong"]
            });
            res.json({
                cart: newUser.cart
            });
        });
    });
});


router.use((req, res, next) => {
    const { product, _id } = req.body;
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(401).json({
        errors: ["Invalid account data"]
    });
    let valid = true;
    const standardProduct = {
        rate: null, // number
        title: null, // string
        _id: null, // string
        price: null, // number
        amount: null, // number
        describtion: null, // string
        image: null, // string
        owner: null, // string,
        ownerId: null, // string
        ownerImg: null, // string
        date: null // string
    };
    for (const key in standardProduct) {
        standardProduct[key] = (product || {})[key];
        valid = valid && standardProduct[key] ? true : false;
        if (key === "rate" || key === "price" || key === "amount") {
            valid = valid && typeof standardProduct[key] === "number";
        } else {
            valid = valid && typeof standardProduct[key] === "string";
        }
    }
    if (!valid) return res.json({
        errors: ["Invalid item to add"]
    });
    req.body.standardProduct = standardProduct;
    next();
});


router.post("/add", (req, res) => {
    const { standardProduct, _id } = req.body;
    console.log(standardProduct, _id);
    User.findById(_id, (err, user) => {
        if (err) return res.status(500).json({
            errors: ["Something went wrong"]
        });
        if (!user) return res.status(401).json({
            errors: ["Invalid account data"]
        });
        user.cart = removeDuplicates([...user.cart, standardProduct], '_id');
        user.save((err2, newUser) => {
            if (err2) return res.status(500).json({
                errors: ["Something went wrong"]
            });
            res.json({
                cart: newUser.cart
            });
        });
    });
});

router.post("/remove", (req, res) => {
    const { standardProduct, _id } = req.body;
    User.findById(_id, (err, user) => {
        if (err) return res.status(500).json({
            errors: ["Something went wrong"]
        });
        if (!user) return res.status(401).json({
            errors: ["Invalid account data"]
        });
        user.cart = removeDuplicates([...user.cart, standardProduct], '_id');
        for (let i = 0; i < user.cart.length; i++) {
            if (user.cart[i]._id === standardProduct._id) {
                user.cart.splice(i, 1);
                break;
            }
        }
        user.save((err2, newUser) => {
            if (err2) return res.status(500).json({
                errors: ["Something went wrong"]
            });
            res.json({
                cart: newUser.cart
            });
        });
    });
});

module.exports = router;