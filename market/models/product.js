const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const product = new Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    describtion: {
        type: String,
        required: true
    },
    rate: {
        type: Number,
        default: 0
    },
    image: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    owner: {
        type: String,
        required: true
    },
    ownerId: {
        type: String,
        required: true
    },
    ownerImg: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

mongoose.model("product", product);