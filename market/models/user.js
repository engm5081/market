const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const user = new Schema({
    firstName: {
        type: String,
        required: true,
        trim: true,
        minlength: 2
    },
    lastName: {
        type: String,
        required: true,
        trim: true,
        minlength: 2
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    gender: {
        type: String,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    profileImg: {
        type: String,
        default: '("http://placehold.it/1500x350/34495e'
    },
    image: {
        type: String,
        default: 'http://placehold.it/150x150/34495e'
    },
    wishList: {
        type: Array,
        default: []
    },
    cart: {
        type: Array,
        default: []
    },
    date: {
        type: Date,
        default: Date.now
    }
});

mongoose.model("user", user);